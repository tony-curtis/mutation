#ifndef _MUTATE_H
#define _MUTATE_H 1

void init_seq(char *old, char *nova, int me, int receiver);
void mutate(char *nova, int pe, int receiver);

#endif  /* _MUTATE_H */
