#include "options.h"
#include "mutate.h"
#include "inspect.h"

#include <iostream>
#include <unistd.h>

#include <cstdlib>

#include <chrono>

#include <shmem.h>

static const int RECEIVER = 0;    // PE that gets the updates

int
main(int argc, char *argv[])
{
    using namespace std::chrono;

    shmem_init();
    const int me = shmem_my_pe();

    fill_options(argc, argv, me, RECEIVER);

    char *preserve = (char *) shmem_malloc(cli.seqlen);
    if (preserve == NULL) {
        if (me == RECEIVER) {
            std::cerr << "malloc(preserve) failed\n";
        }
        shmem_global_exit(1);
    }

    char *seq = (char *) shmem_malloc(cli.seqlen);
    if (seq == NULL) {
        if (me == RECEIVER) {
            std::cerr << "malloc(seq) failed\n";
        }
        shmem_global_exit(1);
    }

    show_options(me, RECEIVER);

    const auto start = high_resolution_clock::now();

    init_seq(preserve, seq, me, RECEIVER);

    inspect(preserve, seq, me, RECEIVER);

    // ready
    shmem_barrier_all();

    // fire off changes
    mutate(seq, me, RECEIVER);

    shmem_barrier_all();
    // done

    inspect(preserve, seq, me, RECEIVER);

    const auto end = high_resolution_clock::now();

    const auto duration = duration_cast<microseconds>(end - start);

    if (me == RECEIVER) {
        std::cout << "PE " << me << " took " << duration.count() << " us\n";
    }

    // clean up
    shmem_free(seq);
    shmem_free(preserve);

    shmem_finalize();

    return 0;
}
