#include "options.h"
#include "ansi.h"

#include <iostream>
#include <iomanip>

#include <cstddef>
#include <cstdlib>

#include <shmem.h>

//
// mark changes
//
static void
highlight(char c, bool do_it)
{
    if (do_it) {
        std::cout << BOLD_YELLOW << c << RESET;
    }
    else {
        std::cout << c;
    }
}

void
inspect(const char *old, char *nova, int me, int receiver)
{
    if (me == receiver) {
        // show nova sequence with changes
        size_t i = 0;

        while (i < cli.seqlen) {
            highlight(nova[i], old[i] != nova[i]);

            ++i;
            if ((i % cli.wrap) == 0) {
                std::cout << "\n";
            }
        }
        std::cout << "\n\n";

        // show changes?
        if (cli.verbose) {
            for (size_t n = 0; n < cli.seqlen; ++n) {
                if (old[n] != nova[n]) {
                    std::cout << std::setw(4) << n << ": " <<
                        old[n] << " -> " << nova[n] << "\n";
                }

            }
        }
    }
}
