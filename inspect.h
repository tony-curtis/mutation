#ifndef _INSPECT_H
#define _INSPECT_H 1

void inspect(const char *old, char *nova, int me, int receiver);

#endif  /* _INSPECT_H */
