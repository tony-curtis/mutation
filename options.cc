#include "options.h"

#include <iostream>
#include <iomanip>

#include <getopt.h>
#include <libgen.h>

#include <cstdlib>
#include <cstring>
#include <unordered_map>

static char *progname;

cli_t cli;                      // exposed

static struct option opts[] = {
    { "resilience", required_argument, NULL, 'r' },
    { "exposures",  required_argument, NULL, 'e' },
    { "seqlen",     required_argument, NULL, 's' },
    { "wrap",       required_argument, NULL, 'w' },
    { "verbose",    no_argument,       NULL, 'v' },
    { NULL,         no_argument,       NULL, 0   }
};

static std::unordered_map<char, const char *> desc = {
    { 'r', "likelihood of change" },
    { 'e', "number of rounds per PE" },
    { 's', "number of bases in sequence" },
    { 'w', "chop sequence lines at column N" },
    { 'v', "include change listing at end" }
};

static const int padding = 18;  // formatting width pad

static void
bail()
{
    using namespace std;

    cerr << "\n";
    cerr << "Usage: " << progname << " [options]\n";
    cerr << "\n";

    struct option *op = (struct option *) opts;

    while (op->name != NULL) {
        const bool takes = (op->has_arg == required_argument);
        const char c = (char) op->val;
        const int n = strlen(op->name);

        cerr << "    -" << c << " " << string(takes ? "N" : " ") <<
            " | --" << op->name << string(takes ? "=N" : "  ") <<
            setw(padding - n) <<
            "    " << desc[c] << "\n";
        ++op;
    }

    cerr << "\n";
}

void
fill_options(int argc, char *argv[], int me, int receiver)
{
    progname = basename(argv[0]);

    // defaults
    cli.resilience = 1000;
    cli.exposures = 1000;
    cli.seqlen = 280;
    cli.wrap = 72;
    cli.verbose = false;

    while (1) {
        const int c =
            getopt_long(argc, argv,
                        // initial colon suppresses error messaages
                        ":r:e:s:w:v",
                        opts, NULL);

        if (c == -1) {
            break;
        }

        switch (c) {
        case 'r':
            cli.resilience = std::stol(optarg);
            break;
        case 'e':
            cli.exposures = std::stol(optarg);
            break;
        case 's':
            cli.seqlen = std::stol(optarg);
            break;
        case 'w':
            cli.wrap = std::stol(optarg);
            break;
        case 'v':
            cli.verbose = true;
            break;
        default:
            if (me == receiver) {
                bail();
                exit(1);
            }
            break;
        }
    }
}

static void
hr()
{
    using namespace std;

    for (size_t i = 0; i < cli.wrap; ++i) {
        cout << '-';
    }
    cout << "\n";
}

void
show_options(int me, int receiver)
{
    if (me == receiver) {
        using namespace std;

        const string vp = cli.verbose ? "yes" : "no";

        cout << "\n";
        hr();
        cout << "Resilience      (-r)    = " << cli.resilience << "\n";
        cout << "Exposures       (-e)    = " << cli.exposures << "\n";
        cout << "Sequence Length (-s)    = " << cli.seqlen << "\n";
        cout << "Wrap Line       (-w)    = " << cli.wrap << "\n";
        cout << "Verbose?        (-v)    = " << vp << "\n";
        hr();
        cout << "\n";
    }
}
