# Usage

    -r N | --resilience=N            likelihood of change
    -e N | --exposures=N             number of rounds per PE
    -s N | --seqlen=N                number of bases in sequence
    -w N | --wrap=N                  chop sequence lines at column N
    -v   | --verbose                 include change listing at end


| Option                          | Default |
|---------------------------------|---------|
| likelihood of change            | 1000    |
| number of rounds per PE         | 1000    |
| number of bases in sequence     | 200     |
| chop sequence lines at column N | 72      |
| include change listing at end   | false   |
